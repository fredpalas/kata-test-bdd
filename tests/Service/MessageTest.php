<?php


namespace App\Tests\Service;


use App\Service\Console;
use PHPUnit\Framework\TestCase;

class MessageTest extends TestCase
{

    private $console;

    public function setUp()
    {
        $this->console = new Console();
    }

    function testAliceCanPublishMessage()
    {
        $this->givenAliceWritesOnConsole('user Alice');
        $this->thenAliceWritesOnConsole("Publish Hello World");
        $this->thenAliceWritesOnConsole("timeline");
        $this->thenTheMessageShouldAppear("Hello World");
    }

    function testAliceCanPublishMessageButNotReadIsNotWritten()
    {
        $this->givenAliceWritesOnConsole('user Alice');
        $this->givenAliceWritesOnConsole("Publish Hello World");
        $this->thenTheMessageShouldNotAppear("Hello World");
    }

    function testAliceCanPublishMessageAndBobReadAliceTimeline()
    {
        $this->givenAliceWritesOnConsole('user Alice');
        $this->thenAliceWritesOnConsole("Publish Hello World");
        $this->andBobWriteOnConsole('user Bob');
        $this->andBobWriteOnConsole("timeline Alice");
        $this->thenTheMessageShouldAppear("Hello World");
    }

    function testCharlieCanSubscribeToAliceAndBobTimelineAndViewAllAggregatedListSubscriptions()
    {
        $this->givenCharlieWriteOnConsole("user Charlie");
        $this->thenCharlieWriteOnConsole("subscribe Alice");
        $this->thenCharlieWriteOnConsole("subscribe Bob");
        $this->thenCharlieWriteOnConsole("subscriptions list");
        $this->thenTheMessageShouldAppear("You are subscribe to Alice");
        $this->thenTheMessageShouldAppear("You are subscribe to Bob");
    }

    function testCharlieCanSubscribeToAliceAndNotToBobTimelineAndViewAllAggregatedListSubscriptions()
    {
        $this->givenCharlieWriteOnConsole("user Charlie");
        $this->thenCharlieWriteOnConsole("subscribe Alice");
        $this->thenCharlieWriteOnConsole("subscriptions list");
        $this->thenTheMessageShouldAppear("You are subscribe to Alice");
        $this->thenTheMessageShouldNotAppear("You are subscribe to Bob");
    }

    function testCharlieCanSubscribeToAliceAndBobTimelineAndNotViewAllAggregatedListSubscriptions()
    {
        $this->givenCharlieWriteOnConsole("user Charlie");
        $this->thenCharlieWriteOnConsole("subscribe Alice");
        $this->thenCharlieWriteOnConsole("subscribe Bob");
        $this->thenTheMessageShouldNotAppear("You are subscribe to Alice");
        $this->thenTheMessageShouldNotAppear("You are subscribe to Bob");
    }

    function testBobCanLinkToCharlieInAMessageUsingAt()
    {
        $this->givenCharlieWriteOnConsole("user Bob");
        $this->givenBobWriteOnConsole("Publish Hello World @Charlie");
        $this->thenCharlieWriteOnConsole("user Charlie");
        $this->thenCharlieWriteOnConsole("timeline mentions");
        $this->thenTheMessageShouldAppear("Hello World @Charlie");
    }

    private function givenAliceWritesOnConsole(string $string): void
    {
        $this->writeConsole($string);
    }

    private function andBobWriteOnConsole(string $string)
    {
        $this->givenAliceWritesOnConsole($string);
    }

    private function thenTheMessageShouldAppear(string $string)
    {
        $result = $this->console->getContent();
        $this->assertContains($string,$result);
    }

    private function thenTheMessageShouldNotAppear(string $string)
    {
        $result = $this->console->getContent();
        $this->assertNotContains($string, $result ?? '');
    }

    private function givenCharlieWriteOnConsole(string $string)
    {
        $this->writeConsole($string);
    }

    private function thenCharlieWriteOnConsole(string $string)
    {
        $this->givenCharlieWriteOnConsole($string);
    }

    private function givenBobWriteOnConsole(string $string)
    {
        $this->writeConsole($string);
    }

    private function thenAliceWritesOnConsole(string $string)
    {
        $this->writeConsole($string);
    }

    private function writeConsole($string)
    {
        $this->console->write($string);
    }

}