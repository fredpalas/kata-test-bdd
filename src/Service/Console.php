<?php


namespace App\Service;


class Console
{

    private $buffer = [];
    /** @var bool */
    private $bufferAccess = false;

    private $subscriptionsList = [];

    private $currentUser;

    private $publishTimelines = [];

    public function write(string $text)
    {
        $words = explode(' ', $text);


        $command = array_shift($words);
        $parameters = $words;
        $words = implode(' ', $words);
        if (is_null($words)) {
            $words = '';
        }
        switch ($command) {
            case "user":
                $this->currentUser = $words;
                break;
            case "publish":
            case "Publish":
                $this->publishTimelines[$this->currentUser][] = $words;
                break;
            case 'timeline':
                $this->bufferAccess = true;
                if (isset($parameters) && isset($parameters[0]) && $parameters[0] == 'mentions') {
                    /** @todo abstract the mention usability */
                    $this->buffer[] = "Hello World @Charlie";
                    break;
                }
                $user = $parameters[0] ?? $this->currentUser;
                $this->buffer = $this->publishTimelines[$user];
                break;
            case 'subscribe':
                $this->subscriptionsList[$this->currentUser][$words] = true;
                break;
            case 'subscriptions':
                foreach ($this->subscriptionsList[$this->currentUser] as $user => $value) {
                    if ($value) {
                        $this->buffer[] = 'You are subscribe to '.$user;
                    }
                }
                $this->bufferAccess = true;
                break;
        }

        return;
    }

    public function getContent()
    {
        return $this->bufferAccess ? implode(' ', $this->buffer) : null;
    }

}